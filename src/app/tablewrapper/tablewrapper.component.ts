import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tablewrapper',
  templateUrl: './tablewrapper.component.html',
  styleUrls: ['./tablewrapper.component.scss']
})
export class TablewrapperComponent implements OnInit {

  @Input('columns') columns: any[];
  @Input('value') value: any[];
  @Input('resize') resize: boolean;
  
  constructor() { }

  ngOnInit() {
  }

}
