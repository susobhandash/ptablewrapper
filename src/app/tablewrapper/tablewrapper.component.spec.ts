import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TablewrapperComponent } from './tablewrapper.component';

describe('TablewrapperComponent', () => {
  let component: TablewrapperComponent;
  let fixture: ComponentFixture<TablewrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TablewrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablewrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
