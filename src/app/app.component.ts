import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ang8';

  cols = [
    { field: 'vin', header: 'Vin', width: '25%'},
    { field: 'year', header: 'Year', width: '15%' },
    { field: 'brand', header: 'Brand', width: '35%' },
    { field: 'color', header: 'Color', width: '25%' }
  ];

  data = [
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
    {vin: 'a1653d4d', year: 1998, brand: 'VW', color: 'White'},
  ];
}
